package ru.t1.kubatov.tm.service;

import ru.t1.kubatov.tm.api.repository.IProjectRepository;
import ru.t1.kubatov.tm.api.service.IProjectService;
import ru.t1.kubatov.tm.enumerated.Sort;
import ru.t1.kubatov.tm.enumerated.Status;
import ru.t1.kubatov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kubatov.tm.exception.field.DescriptionEmptyException;
import ru.t1.kubatov.tm.exception.field.IdEmptyException;
import ru.t1.kubatov.tm.exception.field.IndexIncorrectException;
import ru.t1.kubatov.tm.exception.field.NameEmptyException;
import ru.t1.kubatov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public final class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(Comparator<Project> comparator) {
        if (comparator == null) return findAll();
        return projectRepository.findAll(comparator);
    }

    @Override
    public List<Project> findAll(Sort sort) {
        if (sort == null) return findAll();
        final Comparator<Project> comparator = (Comparator<Project>) sort.getComparator();
        if (comparator == null) return findAll();
        return findAll(comparator);
    }

    @Override
    public Project add(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        return projectRepository.add(project);
    }

    @Override
    public void deleteAll() {
        projectRepository.deleteAll();
    }

    @Override
    public boolean existsById(String id) {
        return projectRepository.existsById(id);
    }

    @Override
    public Project create(String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = new Project();
        project.setName(name);
        return projectRepository.add(project);
    }

    @Override
    public Project create(String name, String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return projectRepository.add(project);
    }

    @Override
    public Project findByID(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.findByID(id);
    }

    @Override
    public Project findByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project delete(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        return projectRepository.delete(project);
    }

    @Override
    public Project deleteByID(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.deleteByID(id);
    }

    @Override
    public Project deleteByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return projectRepository.deleteByIndex(index);
    }

    @Override
    public Project updateByID(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Project project = projectRepository.findByID(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Project project = projectRepository.findByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = findByID(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Project project = findByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}
