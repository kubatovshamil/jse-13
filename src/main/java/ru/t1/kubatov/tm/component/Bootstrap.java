package ru.t1.kubatov.tm.component;

import ru.t1.kubatov.tm.api.component.IBootstrap;
import ru.t1.kubatov.tm.api.controller.ICommandController;
import ru.t1.kubatov.tm.api.controller.IProjectController;
import ru.t1.kubatov.tm.api.controller.IProjectTaskController;
import ru.t1.kubatov.tm.api.controller.ITaskController;
import ru.t1.kubatov.tm.api.repository.ICommandRepository;
import ru.t1.kubatov.tm.api.repository.IProjectRepository;
import ru.t1.kubatov.tm.api.repository.ITaskRepository;
import ru.t1.kubatov.tm.api.service.ICommandService;
import ru.t1.kubatov.tm.api.service.IProjectService;
import ru.t1.kubatov.tm.api.service.IProjectTaskService;
import ru.t1.kubatov.tm.api.service.ITaskService;
import ru.t1.kubatov.tm.constant.ArgumentConstant;
import ru.t1.kubatov.tm.constant.CommandConstant;
import ru.t1.kubatov.tm.controller.CommandController;
import ru.t1.kubatov.tm.controller.ProjectController;
import ru.t1.kubatov.tm.controller.ProjectTaskController;
import ru.t1.kubatov.tm.controller.TaskController;
import ru.t1.kubatov.tm.enumerated.Status;
import ru.t1.kubatov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.kubatov.tm.exception.system.CommandNotSupportedException;
import ru.t1.kubatov.tm.model.Project;
import ru.t1.kubatov.tm.model.Task;
import ru.t1.kubatov.tm.repository.CommandRepository;
import ru.t1.kubatov.tm.repository.ProjectRepository;
import ru.t1.kubatov.tm.repository.TaskRepository;
import ru.t1.kubatov.tm.service.CommandService;
import ru.t1.kubatov.tm.service.ProjectService;
import ru.t1.kubatov.tm.service.ProjectTaskService;
import ru.t1.kubatov.tm.service.TaskService;
import ru.t1.kubatov.tm.util.TerminalUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Bootstrap implements IBootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final static Logger LOGGER_LIFECYCLE = LoggerFactory.getLogger("LIFECYCLE");

    private final static Logger LOGGER_COMMANDS = LoggerFactory.getLogger("COMMANDS");

    @Override
    public void run(final String[] args) {
        initDemoData();
        initLogger();
        try {
            parseArguments(args);
            parseCommands();
        } catch (Exception e) {
            LOGGER_LIFECYCLE.error(e.getMessage());
            System.err.println("[FAIL]");
        }
    }

    private void initLogger() {
        LOGGER_LIFECYCLE.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                LOGGER_LIFECYCLE.info("*** TASK MANAGER IS SHUTTING DOWN ***");
            }
        });
    }

    private void initDemoData() {
        projectService.add(new Project("Project1", Status.COMPLETED));
        projectService.add(new Project("Project2", Status.NOT_STARTED));
        projectService.add(new Project("Project3", Status.IN_PROGRESS));
        projectService.add(new Project("Project4", Status.IN_PROGRESS));
        taskService.add(new Task("Task1", "Task1"));
        taskService.add(new Task("Task2", "Task2"));
        taskService.add(new Task("Task3", "Task3"));
        taskService.add(new Task("Task4", "Task4"));
    }

    private void parseArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        LOGGER_COMMANDS.info(arg);
        parseArgument(arg);
    }

    private void parseCommands() {
        commandController.showWelcome();
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            try {
                final String command = TerminalUtil.nextLine();
                LOGGER_COMMANDS.info(command);
                parseCommand(command);
            } catch (Exception e) {
                LOGGER_LIFECYCLE.error(e.getMessage());
                System.out.println("[FAIL]");
            }
        }
    }

    private void parseCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConstant.VERSION:
                commandController.showVersion();
                break;
            case CommandConstant.HELP:
                commandController.showHelp();
                break;
            case CommandConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case CommandConstant.EXIT:
                exit();
                break;
            case CommandConstant.COMMANDS:
                commandController.showCommands();
                break;
            case CommandConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            case CommandConstant.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConstant.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConstant.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConstant.PROJECT_SHOW_BY_ID:
                projectController.showProjectByID();
                break;
            case CommandConstant.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case CommandConstant.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectByID();
                break;
            case CommandConstant.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CommandConstant.PROJECT_DELETE_BY_ID:
                projectController.deleteProjectByID();
                break;
            case CommandConstant.PROJECT_DELETE_BY_INDEX:
                projectController.deleteProjectByIndex();
                break;
            case CommandConstant.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case CommandConstant.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case CommandConstant.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case CommandConstant.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case CommandConstant.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case CommandConstant.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case CommandConstant.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConstant.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConstant.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CommandConstant.TASK_SHOW_BY_ID:
                taskController.showTaskByID();
                break;
            case CommandConstant.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case CommandConstant.TASK_SHOW_BY_PROJECT_ID:
                taskController.showTaskByProjectID();
                break;
            case CommandConstant.TASK_UPDATE_BY_ID:
                taskController.updateTaskByID();
                break;
            case CommandConstant.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case CommandConstant.TASK_DELETE_BY_ID:
                taskController.deleteTaskByID();
                break;
            case CommandConstant.TASK_DELETE_BY_INDEX:
                taskController.deleteTaskByIndex();
                break;
            case CommandConstant.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case CommandConstant.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case CommandConstant.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case CommandConstant.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case CommandConstant.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case CommandConstant.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case CommandConstant.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case CommandConstant.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            default:
                throw new CommandNotSupportedException(command);
        }
    }

    private void parseArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            case ArgumentConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case ArgumentConstant.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                throw new ArgumentNotSupportedException(arg);
        }
        exit();
    }

    private void exit() {
        System.exit(0);
    }

}
