package ru.t1.kubatov.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(final String message) {
        super("Error! Argument \"" + message + "\" not supported...");
    }

}
