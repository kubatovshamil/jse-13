package ru.t1.kubatov.tm.api.service;

import ru.t1.kubatov.tm.api.repository.IProjectRepository;
import ru.t1.kubatov.tm.enumerated.Sort;
import ru.t1.kubatov.tm.model.Project;
import ru.t1.kubatov.tm.enumerated.Status;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IProjectRepository {

    Project create(String name);

    Project create(String name, String description);

    Project updateByID(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    List<Project> findAll(Sort sort);

}
